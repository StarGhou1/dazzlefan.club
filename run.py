from config import Config
from app import create_app
from app.models import db

app = create_app(Config)

if __name__ == '__main__':
    with app.app_context():
        db.create_all()

    app.run(templates_folder='templates')

