from flask import Blueprint

auth_blueprint = Blueprint('auth', __name__, url_prefix='/auth')
story_blueprint = Blueprint('story', __name__, url_prefix='/story')
thread_blueprint = Blueprint('thread', __name__, url_prefix='/thread/')
misc_blueprint = Blueprint('misc', __name__, url_prefix='/')
