from flask import current_app, render_template, redirect, request, url_for
from flask_login import login_user, logout_user, login_required
from app import Admin
from app.forms.admin import AdminForm
from app.forms.current_thread import CurrentThreadForm
from app.forms.discord import DiscordLinkForm
from app.routes import auth_blueprint


@auth_blueprint.route('/login', methods=['GET', 'POST'])
def login():
    form = AdminForm(request.form)

    if form.validate_on_submit():
        admin = Admin.query.filter_by(username=form.username.data).first()
        if admin is None or not admin.check_password(form.password.data):
            form.username.errors.append('Invalid credentials.')
        else:
            login_user(admin)
            return redirect(url_for('misc.index'))

    return render_template('admin/login.html', form=form)


@auth_blueprint.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('misc.index'))


@auth_blueprint.route('/set_thread', methods=['GET', 'POST'])
@login_required
def set_thread():
    form = CurrentThreadForm(request.form)

    if form.validate_on_submit():
        current_app.redis.set('current_thread', form.thread_url.data)
        return redirect(url_for('misc.index'))

    return render_template('misc/set_thread.html', form=form)


@auth_blueprint.route('/set_discord', methods=['GET', 'POST'])
@login_required
def set_discord():
    form = DiscordLinkForm(request.form)

    if form.validate_on_submit():
        current_app.redis.set('discord', form.discord_link.data)
        return redirect(url_for('misc.index'))

    return render_template('misc/set_discord.html', form=form)
