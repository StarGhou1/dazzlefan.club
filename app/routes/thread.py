from flask import abort, redirect, render_template, request, url_for
from flask_login import login_required
from app.forms.thread import ThreadInputForm
from app.models.thread import ArchivedThread
from app.routes import thread_blueprint


@thread_blueprint.route('/list')
def archive():
    threads = ArchivedThread.get_threads()
    return render_template("thread/listing.html", threads=threads)


@thread_blueprint.route('/add', methods=['GET', 'POST'])
@login_required
def add():
    form = ThreadInputForm(request.form)

    if form.validate_on_submit():
        new_thread_number = form.number.data
        if ArchivedThread.find_by_number(new_thread_number):
            form.number.errors.append('A thread with that number is already archived.')
        else:
            thread = ArchivedThread(new_thread_number, form.link.data)
            thread.save()
            return redirect(url_for('thread.archive'))

    return render_template('thread/post.html', form=form)


@thread_blueprint.route('/delete/<id>', methods=['POST'])
@login_required
def delete(id):
    thread = ArchivedThread.find_by_id(id)

    if not thread:
        abort(404)
    thread.delete()

    return redirect(url_for('thread.archive'))
