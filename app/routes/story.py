from flask import abort, redirect, render_template, request, Response, url_for
from flask_login import login_required
from app.forms.story import StoryUploadForm
from app.models.story import Story
from app.routes import story_blueprint


@story_blueprint.route('/archive')
def archive():
    stories = Story.get_stories()
    return render_template("story/listing.html", stories=stories)


@story_blueprint.route('/submit', methods=['GET', 'POST'])
@login_required
def submit():
    form = StoryUploadForm(request.form)

    if form.validate_on_submit():
        story = Story(form.title.data, form.author.data, form.content.data, form.ponepaste_link.data)
        story.save()
        return redirect(url_for('story.view', id=story.id))

    return render_template('story/post.html', form=form)


@story_blueprint.route('/edit/<id>', methods=['GET', 'POST'])
@login_required
def edit(id):
    form = StoryUploadForm(request.form)
    story = Story.find_by_id(id)

    if story is None:
        abort(404)

    if request.method == "GET":
        form.title.data = story.title
        form.author.data = story.author
        form.content.data = story.content
        form.ponepaste_link.data = story.ponepaste_link

    if form.validate_on_submit():
        story.update(form.title.data, form.author.data, form.content.data, form.ponepaste_link.data)
        return redirect(url_for('story.view', id=story.id))

    return render_template('story/edit.html', form=form, id=story.id)


@story_blueprint.route('/delete/<id>', methods=['POST'])
@login_required
def delete(id):
    story = Story.find_by_id(id)

    if story is None:
        return abort(404)
    story.delete()

    return redirect(url_for('story.archive'))


@story_blueprint.route('/download/<id>', methods=['GET'])
def download(id):
    story = Story.find_by_id(id)

    if story is None:
        abort(404)

    return Response(story.content, mimetype="text/plain", headers={
        "Content-Disposition": f"attachment;filename={story.title}.txt"
    })


@story_blueprint.route('/view/<id>')
def view(id):
    story = Story.find_by_id(id)

    if story is None:
        abort(404)

    return render_template('story/view.html', story=story.story_dict())
