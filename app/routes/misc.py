from os import listdir
from random import choice
from flask import abort, current_app, redirect, render_template, url_for
from app.routes import misc_blueprint


@misc_blueprint.route('/')
def index():
    image_names = listdir(f'{current_app.static_folder}/images/index')
    image_filename = choice(image_names)
    return render_template('index.html', img=url_for('static', filename=f'images/index/{image_filename}'))


@misc_blueprint.route('/current_thread')
def current_thread():
    thread_link = current_app.redis.get('current_thread')

    if not thread_link:
        abort(404)

    return redirect(thread_link)


@misc_blueprint.route('/discord')
def discord():
    discord_link = current_app.redis.get('discord')

    if not discord_link:
        abort(404)

    return redirect(discord_link)
