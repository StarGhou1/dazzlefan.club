from flask import Flask, render_template
from flask_login import LoginManager
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address
from flask_paranoid import Paranoid
from flask_redis import FlaskRedis
from flask_talisman import Talisman
from flask_wtf import CSRFProtect
from app.models import db
from app.models.admin import Admin
from app.routes.story import story_blueprint, download
from app.routes.thread import thread_blueprint
from app.routes.misc import misc_blueprint
from app.routes.auth import auth_blueprint, login


def access_denied(e):
    return render_template('misc/403.html'), 403


def page_not_found(e):
    return render_template('misc/404.html'), 404


def internal_server_error(e):
    return render_template('misc/500.html'), 500


def ratelimit_handler(e):
    return render_template('misc/429.html'), 429


def create_app(config_name):
    app = Flask(__name__, static_folder='static', static_url_path='/static')
    limiter = Limiter(key_func=get_remote_address)
    app.config.from_object(config_name)

    db.init_app(app)
    redis = FlaskRedis(app)
    login_manager = LoginManager()
    login_manager.init_app(app)
    Paranoid(app)
    limiter.init_app(app)
    limiter.limit("25/hour")(download)
    limiter.limit("5/hour", methods=['POST'])(login)

    CSRFProtect(app)
    Talisman(app, content_security_policy={
        'default-src': ['\'self\'', '*.google.com', '*.bootstrapcdn.com', '*.datatables.net', '*.googleapis.com',
                        '*.gstatic.com', '*.w3.org'],
        'img-src': ['\'self\'', 'data:']
    })

    app.redis = redis
    app.register_blueprint(story_blueprint)
    app.register_blueprint(auth_blueprint)
    app.register_blueprint(misc_blueprint)
    app.register_blueprint(thread_blueprint)
    app.register_error_handler(403, access_denied)
    app.register_error_handler(404, page_not_found)
    app.register_error_handler(429, ratelimit_handler)
    app.register_error_handler(500, internal_server_error)

    @login_manager.user_loader
    def user_loader(id):
        return Admin.query.get(id)

    @login_manager.unauthorized_handler  # ???
    def unauthorized():
        return render_template('misc/403.html'), 403

    return app
