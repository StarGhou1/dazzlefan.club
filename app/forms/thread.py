from flask_wtf import FlaskForm
from wtforms import validators
from wtforms.fields import IntegerField, SubmitField, StringField


class ThreadInputForm(FlaskForm):
    number = IntegerField(validators=[
        validators.input_required("Number required."),
        validators.NumberRange(min=0)])

    link = StringField(validators=[
        validators.input_required("Link required."),
        validators.URL()])

    submit = SubmitField('Submit')
