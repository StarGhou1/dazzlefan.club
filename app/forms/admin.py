from flask_wtf import FlaskForm, RecaptchaField
from wtforms import validators
from wtforms.fields import SubmitField, PasswordField, StringField


class AdminForm(FlaskForm):
    username = StringField(validators=[
        validators.input_required("Username required."),
        validators.Length(min=5, max=32)])

    password = PasswordField(validators=[
        validators.input_required("Password required."),
        validators.Length(min=5, max=256)])

    recaptcha = RecaptchaField()

    submit = SubmitField('Submit')
