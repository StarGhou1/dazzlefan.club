from flask_wtf import FlaskForm
from wtforms import validators
from wtforms.fields import SubmitField, StringField


class DiscordLinkForm(FlaskForm):
    discord_link = StringField(validators=[
        validators.input_required("Discord Link required."),
        validators.length(min=1, max=64),
        validators.URL()])

    submit = SubmitField('Submit')
