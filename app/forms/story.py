from flask_wtf import FlaskForm
from wtforms import validators
from wtforms.fields import StringField, SubmitField, TextAreaField


class StoryUploadForm(FlaskForm):
    title = StringField(validators=[
        validators.input_required("Title required."),
        validators.Length(min=4, max=128, message="Title must be between 4 and 128 characters long.")])

    author = StringField(validators=[
        validators.input_required("Author required."),
        validators.Length(min=1, max=64, message="Author name must be between 1 and 64 characters long.")])

    ponepaste_link = StringField(render_kw={"placeholder": "Leave blank if none"})

    content = TextAreaField(validators=[
        validators.InputRequired("Stories cannot be empty."),
        validators.Length(min=1)])

    submit = SubmitField('Submit')
