from flask_wtf import FlaskForm
from wtforms import validators
from wtforms.fields import SubmitField, StringField


class CurrentThreadForm(FlaskForm):
    thread_url = StringField(validators=[
        validators.input_required("Thread Link required."),
        validators.URL(),
        validators.length(min=1, max=64,)])

    submit = SubmitField('Submit')
