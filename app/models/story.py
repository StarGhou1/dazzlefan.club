from datetime import datetime
from app.models import db


class Story(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    title = db.Column(db.String, unique=False, nullable=False)
    author = db.Column(db.String, unique=False, nullable=False)
    submission_date = db.Column(db.Date, nullable=False, default=datetime.now)
    edited_date = db.Column(db.Date, nullable=False, default=datetime.now)
    content = db.Column(db.Text, unique=False, nullable=False)
    ponepaste_link = db.Column(db.String, unique=False, nullable=True)

    def story_dict(self):
        return {
            'id': self.id,
            'title': self.title,
            'author': self.author,
            'submission_date': self.submission_date,
            'edited_date': self.edited_date,
            'content': self.content,
            'ponepaste_link': self.ponepaste_link
        }

    def save(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    def update(self, title, author, content, ponepaste_link=""):
        self.title = title
        self.author = author
        self.content = content
        self.ponepaste_link = ponepaste_link
        self.edited_date = datetime.now()
        db.session.commit()

    @classmethod
    def get_stories(cls):
        return cls.query.all()

    @classmethod
    def find_by_id(cls, id):
        return cls.query.filter_by(id=id).first()

    def __init__(self, title, author, content, ponepaste_link=""):
        self.title = title
        self.author = author
        self.content = content
        self.ponepaste_link = ponepaste_link
