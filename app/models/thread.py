from app.models import db


class ArchivedThread(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    thread_num = db.Column(db.Integer, unique=True, nullable=False)
    thread_link = db.Column(db.String, unique=True, nullable=False)

    def save(self):
        db.session.add(self)
        db.session.commit()

    @classmethod
    def get_threads(cls):
        return cls.query.all()

    @classmethod
    def find_by_id(cls, id):
        return cls.query.filter_by(id=id).first()

    @classmethod
    def find_by_number(cls, thread_number):
        return cls.query.filter_by(thread_num=thread_number).first()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    def __init__(self, thread_num, thread_link):
        self.thread_num = thread_num
        self.thread_link = thread_link
